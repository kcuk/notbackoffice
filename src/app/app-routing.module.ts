import { LoginComponent } from './account/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import {AccountComponent } from './account/account.component';

const routes: Routes = [
  {
    path: '',redirectTo:'main',pathMatch:'full'
  },
  {path:'main',loadChildren:'./main/main.module#MainModule'},
  {
    path: 'admin',component:AccountComponent
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {
  
}

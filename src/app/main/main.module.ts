import { NgModule} from '@angular/core';

import{ MainComponent} from './main.component';
import{ Routes,RouterModule} from '@angular/router'
import { CommonModule } from '@angular/common';


const routes: Routes = [
  {
    path:'',
    component:MainComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes),CommonModule ],
  // exports: [ RouterModule],
  declarations: [MainComponent]
})

export class MainModule {
    constructor(){
        console.log("main module!@@");
 }
}